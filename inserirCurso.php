<html>
    <head>
        <title>Inserir Curso</title>
        <meta charset="utf-8" />
 	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <link rel='icon' href='img/favicon.ico'>
        <link rel="stylesheet" type="text/css" href="Semantic-UI-CSS-master/semantic.min.css">
    </head>
    <body>
        <?php
            require_once("menu.php");
        ?>
        <div class="pusher">
        <br>
        <?php
            require_once("menuPrincipal.php");
        ?>
        <div class='ui main text container'>
        <br>
        <form method="post" action="inserirCursoProc.php" class="ui form">
            <div class="ui inverted green segment">
                <div class="ui inverted form">
                    <div class="two fields">
                    <div class="field">
                        <label>Nome:</label>
                        <input placeholder="Digite o nome do Curso...." type="text" name="curso">
                    </div>
                    <div class="inline fields">
                        <label>Status do Curso:</label>
                        <input tabindex="0" class="hidden" type="radio" name="status" value="ativo">
                        <label>Ativo</label>
                        <input tabindex="0" class="hidden" type="radio" name="status" value="desativo">
                        <label>Desativo</label><br><br>
                    </div>
                    </div>
                    <button class="fluid ui button" type="submit">Enviar</button>
                </div>
            </div>
        </form>
    <br><br>
    </div>
    </div>
    </body>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/semantic.js"></script>
    <script type="text/javascript" src="js/app.js"></script>
</html>